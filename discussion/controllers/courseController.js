const Course = require("../models/course");
/*
module.exports.addCourse = (req) =>{
	// Uses the information from the request body to provide all the necessary information.

	if(req.isAdmin === true){


	let new_Course = new Course({
		name: req.name,
		description : req.description,
		price : req.price
	});

		return new_Course.save().then((new_Course, err) =>{
			if (err){
				return false;
			}return{
				message : 'New Course created'
			}
		})

	}

}



let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	})*/

	module.exports.addCourse = (data) => {
	if(data.isAdmin){
		let new_course = new Course ({
			name: data.course.name,
			description: data.course.description,
			price: data.course.price
		})

		return new_course.save().then((new_course, error) => {
			if(error){
				return false
			}

			return {
				message: 'New course successfully created!'
			}
		})
	} 

	let message = Promise.resolve('User must me Admin to Access this.')

	return message.then((value) => {
		return value

	// you can use this to simplify code
	// Promise.resolve('User must me Admin to Access this.')
	})
		
}


// ACTIVITY 

// Controller function that retrieving all the courses


module.exports.getCourse = (req, res) =>{
	return Course.find({}).then(result => {
		return result;
	})
}


module.exports.updateCourse = (reqParams, reqBody) =>{

	// specify fields/properties of the docs to be updated

	let updatedCourse = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	};

	/*
		Syntax:

		findByIdAndUpdate(documentID, updatesToBeapplied)
	*/

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=>{

		if(error){
			return false;
		}else{
			return {
				message : "Course Updated successfully"
			}
		}


	})

}


module.exports.archiveCourse = (reqParams) =>{

	let updateActiveField = {
		isActive : false
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error)=>{

		if(error){
			return false
		}else{
			return {
				message : "Course Archived successfully"
			}
		}

	})
}


module.exports.getActive = () =>{
	return Course.find({ isActive : true})
}


module.exports.unarchiveCourse = (reqParams) =>{

	let updateActiveField = {
		isActive : true
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error)=>{

		if(error){
			return false
		}else{
			return {
				message : "Course Unarchived successfully"
			}
		}

	})
}

