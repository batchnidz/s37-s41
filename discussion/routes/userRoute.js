const express = require("express")
const router = express.Router();
const auth = require("../auth");


const userController = require("../controllers/userController")

// Check email
router.post("/checkEmail",(req,res)=>{

	userController.checkEmailExist(req.body).then(result => res.send(result))
})

// Route for registration

router.post("/register", (req,res)=>{
	userController.registerUser(req.body).then(result => res.send(result))
})


// ***activity
// Route for retrieving user details
router.post("/details", (req, res) => {

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});

// Route for user authentication
router.post("/login", (req,res)=>{
	userController.loginUser(req.body).then(result => res.send(result))
});

// Route for enrolling an authenticated user

router.post("/enroll", auth.verify, (req,res)=>{

	let data = {
		// User Id will be retrieved from the req.header
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId

	}

	userController.enroll(data).then(result => res.send(result))

})

module.exports = router;