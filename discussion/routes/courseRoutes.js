const express = require("express")
const router = express.Router();
const auth = require("../auth")

const courseController = require("../controllers/courseController");

// Router for creating a course

router.post("/addCourse", auth.verify, (req, res) =>{

	const data = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(result => res.send(result));
});



router.get("/getcourse", (req,res) =>{

	courseController.getCourse().then(resultFromController => res.send(resultFromController));


})

// Route for updating the course

router.put("/:courseId", auth.verify,(req,res)=>{
	courseController.updateCourse(req.params, req.body).then(result => res.send(result))

		
})

// Routes for archive
// Using params
router.put("/archive/:courseId", auth.verify, (req,res)=>{
		courseController.archiveCourse(req.params).then(result => res.send(result))
})


router.put("/unarchive/:courseId", auth.verify, (req,res)=>{
		courseController.unarchiveCourse(req.params).then(result => res.send(result))
})


// users only getting courses active only

router.get("/getAllActive", (req, res)=>{

	courseController.getActive().then(result => res.send(result))
})

/*
	Plan for Capstone:

	Admin
	Users
*/


module.exports = router;

// [ACTIVITY]

// route function that retrieving all the courses





// [ACTIVITY 2]



/*

1. Configure NPM
2. Configure Mongoose
3. Listen for port
4. MRC - Folder
5. Models first for schema and Logic
6. Routes for the API request and Respond for URL 
7. Controller for the functionalities 
8 Dont forget to import router 
11. import models on controllers for schema
12. Add ons dont forget for jsonwebtoken, cors
sinulat ko lng sinabi mo sir

*/